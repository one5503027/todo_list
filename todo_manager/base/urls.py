from django.urls import path
from .views import TodoList, TodoDetail, TodoCreate, TodoUpdate, DeleteView, CustomLoginView, RegisterPage, ProductListView #ProfileUpdateView
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    # path('sort-by-complete/', ProductListView.as_view(), {'sort_by': '-complete'}, name='product_list_sort_by_complete'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('register/', RegisterPage.as_view(), name='register'),
    path('', TodoList.as_view(), name='todos'),
    path('todo/<int:pk>/', TodoDetail.as_view(), name='todo'),
    # path('<int:pk>/profile', EditProfile.as_view(), name='edit-profile'),
    path('todo-create/', TodoCreate.as_view(), name='todo-create'),
    path('todo-update/<int:pk>/', TodoUpdate.as_view(), name='todo-update'),
    path('todo-delete/<int:pk>/', DeleteView.as_view(), name='todo-delete'),
]