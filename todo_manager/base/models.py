from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.dispatch import receiver
from django.db.models.signals import post_save

# from modules.services.utils import unique_slugify
# Create your models here.

class Todo(models.Model):
    PRIORITY_CONST = (
        ('0', 'no_prior'),
        ('1', 'minor'),
        ('2', 'major'),
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=200, verbose_name='Заголовок')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    complete = models.BooleanField(default=False, verbose_name='Выполнено?')
    due_date = models.DateTimeField(auto_now_add=False, verbose_name='Планируемая дата (ММ/ДД/ГГГГ)')
    priority = models.CharField(max_length=10, choices=PRIORITY_CONST, default='NO_PRIOR', verbose_name='Приоритет')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    
    class Meta:
        ordering = ['complete']


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)