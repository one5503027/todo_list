# from django.contrib.auth.models import User
# from django.views.generic import DetailView

# class ProfileView(DetailView):
#     model = User
#     template_name = 'templates/profile.html'
#     context_object_name = 'profile'


from django.views.generic import DetailView
from .models import Profile

class ProfileDetailView(DetailView):
    model = Profile
    template_name = 'profiles/profile.html'
    context_object_name = 'profile'
